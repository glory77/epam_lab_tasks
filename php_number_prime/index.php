<?php

print_r(php_number_prime(10000));

function php_number_prime($n)
{
$result = array();

$sqrt = floor(sqrt($n));

$a = array_fill(2, $n-1, true);

for($i=2;$i<=$sqrt;$i++){
	if($a[$i]===true){
		for($j=$i*$i; $j<=$n; $j+=$i){
			$a[$j]=false;
		}
	}
}

for ($i=2; $i<$n; $i++) {
	if ($a[$i]==true) {
		$result[] = $i;
	}
}

return $result;
}
