<?php

echo php_dynamic_calculator('+', 1, '23', 'xd') . '<br>';
echo php_dynamic_calculator('*', 2, 'hg', 2, 3) . '<br>';
echo php_dynamic_calculator('-', '2', '5', '5') . '<br>';
echo php_dynamic_calculator('/', 1, 4, 'test') . '<br>';
echo php_dynamic_calculator(0, 16, 4, 2);

function php_dynamic_calculator() {
	$result = false;

	$args =  func_get_args();
	$operator = array_shift($args);

	if (is_numeric($operator)) {
		return $operator;
	}

	foreach ($args as $arg) {
		if (!is_numeric($arg)) {
			continue;
		}

		if ($result===false) {
			$result = $arg;
			continue;
		}

		switch ($operator) {
			case '+':
				$result += $arg;
				break;

			case '-':
				$result -= $arg;
				break;

			case '*':
				$result *= $arg;
				break;

			case '/':
				$result /= $arg;
				break;
		}
	}

	return $result;
}
