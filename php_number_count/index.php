<?php

$string = '1sdw2efrg3z';

echo php_number_count($string);

function php_number_count($string)
{
	if (!is_string($string)) {
		return false;
	}

	// gets numbers from string
	// use patten ([0-9]) for digits
	preg_match_all('/([0-9]+)/', $string, $m);

	$result = array_sum($m[1]);

	return $result;
}

