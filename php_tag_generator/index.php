<?php

interface TagInterface {
	static function initialize($className, $tagname);

	function setTagName($tagName);

	function getTagName();

	function setAttribute($name, $value);

	function getAttribute();

	function generateTag();
}

abstract class Tag implements TagInterface {
	protected $attribute;
	protected $tagname;
	protected $text;

	static function initialize($classname, $tagname) {
		$cln = new $classname;
		$cln->setTagName($tagname);
		return $cln;
	}

	function setTagName($name) {
		$this->tagname = $name;
		return $this;
	}

	function getTagName() {
		return $this->tagname;
	}

	function setAttribute($name, $value) {
		$this->attribute[$name] = $value;
		return $this;
	}

	function getAttribute() {
		return $this->attribute;
	}

	function setText($text) {
		$this->text = $text;
		return $this;
	}

	function getText() {
		return $this->text;
	}

	abstract function generateTag();
}

class SingleTag extends Tag {
	function generateTag() {
		switch ($this->getTagname()) {
			case 'br':
				return '<br />';
				break;

			case 'img':
				return '<img src="'.$this->attribute['src'].'" alt="" />';
				break;

			default:
				return;
				break;
		}
	}
}

class ParedTag extends Tag {
	function generateTag() {
		switch ($this->getTagname()) {
			case 'h1':
				return '<h1>'.$this->text.'</h1>';
				break;

			case 'a':
				return '<a href="'.$this->attribute['href'].'">'.$this->text.'</a>';
				break;

			case 'p':
				return '<p>'.$this->text.'</p>';
				break;

			default:
				return;
				break;
		}
	}
}

$tag = Tag::initialize('ParedTag', 'h1')->setText('Hello world')->generateTag();
print $tag;

$tag = Tag::initialize('ParedTag', 'a')->setText('welcom')->setAttribute('href', 'http://epam.com')->generateTag();
print $tag;

$tag = Tag::initialize('SingleTag', 'br')->generateTag();
print $tag;

$tag = Tag::initialize('SingleTag', 'img')->setAttribute('src', 'https://png.pngtree.com/element_our/png/20180912/coffee-time-png_91570.jpg')->generateTag();
print $tag;

$tag = Tag::initialize('ParedTag', 'p')->setText('Some text in p tag here')->generateTag();
print $tag;
