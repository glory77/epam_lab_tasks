<?php

$text = 'My credit card number is 0000 0000 0000 0000';
echo sanitizer($text);

$text = 'Hi! Did yor find my number 1234 5313 6323 1453';
echo sanitizer($text);

$text = 'My phone number is +375 29 111 11 11';
echo sanitizer($text);

function sanitizer($text) {
	$result = preg_replace('/([0-9]{4}\s[0-9]{4}\s[0-9]{4}\s[0-9]{4})/', '&lt;removed&gt;', $text);
	return $result;
}
