<?php

interface HomoSapiens {
	public function gender();
}

interface Human extends HomoSapiens {
	public function gender();
}

abstract class Men {
	abstract public function gender();
}

class John extends Men implements Human {
	private $gender = 'men';

	public function gender() {
		return $this->gender;
	}
}

$john = new John();

echo $john->gender();

$hs = $john instanceof HomoSapiens;
echo $hs;
